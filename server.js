'use strict';

const config = require(`./config`);
const app = require(`./app`);
const server = require(`http`).createServer(app);

server.listen(config.port, () => {
  const { port } = server.address();
  console.log(`listening on ${port} with env ${process.env.NODE_ENV}`);
});

server.on(`error`, (error) => {
  console.log(error);
});

process.on(`uncaughtException`, (err) => {
  console.error(`uncaughtException`, err.stack);
  process.exit();
});

process.on(`unhandledRejection`, (err) => {
  console.error(`unhandledRejection`, err.stack);
});
