'use strict';

const mongoose = require(`mongoose`);
const Promise = require(`bluebird`);

const config = require(`../../config/`);
const Async = require(`../services/Async`);
const Utils = require(`../services/Utils`);
const OAuth = require(`../helpers/oauth`);

const Mailer = Promise.promisifyAll(require(`../helpers/mailer-sendgrid`));

const _ = require(`lodash`);

require(`../models/User`);
const User = mongoose.model(`User`);

const myModule = {
  signin: (req, res) => {
    if (!req.user) {
      return res.ok(null, `Login invalid`, 400);
    }

    const response = {
      user: User.toJSON(req.user),
      accessToken: req.token.accessToken,
      refreshToken: req.token.refreshToken,
    };

    return res.ok(response, `Login success`, 200);
  },
};

module.exports = {
  signin: myModule.signin,

  register: Async.route(async (req, res) => {
    const missing = Utils.missingProperty(req.body, [`email`, `password`, `passwordConfirmation`]);
    if (missing) {
      return res.sendError(`Missing body parameter: ${missing}.`);
    }

    if (req.body.password.length < 6) {
      return res.sendError(`Password minimum length is 6 character`);
    }

    if (req.body.password !== req.body.passwordConfirmation) {
      return res.sendError(`Password confirmation must match`);
    }

    const isUserExist = await User.findOne({ email: req.body.email });

    if (isUserExist) {
      return res.sendError(`Email is already exist`);
    }

    const user = new User(req.body);

    user.role = [`user`];

    const saved = await user.save();

    return res.ok(saved.toJSON(), `Register success`, 200);
  }),

  signInWith: Async.route(async (req, res, next) => {
    const missing = Utils.missingProperty(req.body, [`email`, `provider`, `socialId`]);
    if (missing) {
      return res.sendError(`Missing body parameter: ${missing}`);
    }

    if (_.indexOf([`facebook`, `twitter`, `google`], req.body.provider) < 0) {
      return res.sendError(`Unsupported provider`);
    }

    let user = await User.findOne({ email: req.body.email });

    if (!user) {
      config.log(`Create new user \`${req.body.email}\``);

      user = new User({
        email: req.body.email,
        username: req.body.username,
        fullname: req.body.fullname,
      });
    }

    const provider = req.body.provider;
    user.provider[provider] = req.body.socialId;

    if (!user.role) { user.role = [`user`]; }

    await user.save();

    req.body.grant_type = `password`;
    req.body.username = req.body.email;
    req.body.password = {
      provider,
      socialId: req.body.socialId,
    };

    // return next(req, res, next);
    return OAuth.token()(req, res, () => {
      myModule.signin(req, res);
    });
  }),

  requestForgotPassword: Async.route(async (req, res, next) => {
    const missing = Utils.missingProperty(req.query, [`email`]);
    if (missing) {
      return res.sendError(`Missing querystring parameter: ${missing}`);
    }

    const user = await User.find({ email: req.query.email });

    if (!user) {
      return res.sendError(`Email is not registered.`);
    }

    if (!req.query.resend) {
      user.forgot_password_code = Utils.uid(6, `0123456789`);
      await user.save();
    }

    const mailOption = {
      info: {
        from: `Loyalty Club Admin <no-reply@dycode.com>`,
        to: user.email,
        subject: `Email Verification`,
        html: `<h1>Polfaced</h1><p>Here your confirmation code. ${user.forgot_password_code}. Please insert on your app.</p>`,
        customparams: {
          forgotPasswordCode: user.forgot_password_code,
        },
      },
    };

    let response;

    try {
      response = await Mailer.sendAsync(mailOption);

      return res.ok({ message: `Please open your email for confirmation.` });
    } catch (ex) {
      return res.sendError(`Mailer Error`, { message: response.body.message });
    }
  }),

  validateForgotPassword: Async.route(async (req, res, next) => {
    const missing = Utils.missingProperty(req.body, [`email`, `code`]);
    if (missing) {
      return res.sendError(`Missing body parameter: ${missing}`);
    }

    const user = await User.find({ email: req.body.email });

    if (!user) {
      return res.sendError(`Email is not registered.`);
    }

    const response = {
      ismatch: true,
      message: `Validation code is match`,
    };

    if (req.body.code !== user.forgot_password_code) {
      response.ismatch = false;
      response.message = `Validation code is not match`;

      return res.ok(response, `Forgot Password Validation`);
    }

    user.password_hash = User.hashingPassword(req.body.code);
    await user.save();

    response.newPassword = req.body.code;

    return res.ok(response, `Forgot Password Validation`);
  }),

  changePassword: Async.route(async (req, res, next) => {
    const missing = Utils.missingProperty(req.body, [`email`, `passwordCurrent`, `passwordNew`]);
    if (missing) {
      return res.sendError(`Missing body parameter: ${missing}`);
    }

    const user = await User.find({ email: req.body.email });

    if (!user) {
      return res.sendError(`Email is not registered`);
    }

    const isValid = await User.matchingPassword(user.password_hash, req.body.passwordCurrent);

    if (!isValid) {
      return res.sendError(`Current password is not match`);
    }

    if (req.body.passwordNew.length < 6) {
      return res.sendError(`Password minimum length is 6 character`);
    }

    user.password = req.body.passwordNew;

    try {
      await user.save();
    } catch (ex) {
      console.log(ex);

      return res.sendError(`An Error occured`);
    }

    return res.ok(null, `Password has been changed`);
  }),
};

