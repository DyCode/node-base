'use strict';

/* eslint no-param-reassign: 0 */
const config = require(`../../config/${process.env.NODE_ENV || ``}`);
const Utils = require(`../services/Utils`);
const _ = require(`lodash`);

const path = require(`path`);
const fs = require(`fs`);

function checkBase64(raw) {
  return raw && typeof (raw) === `string` && raw.match(/^data:image\/png;base64,/);
}

exports.get = (req, res, next) => {
  if (_.has(req.body, `base64image`)) {
    const raw = req.body.base64image;
    if (!checkBase64(raw)) {
      return next();
    }

    const base64 = raw.replace(/^data:image\/png;base64,/, ``);
    const filename = `${Utils.uid(64)}.png`;
    const filepath = path.join(config.photoDir, filename);

    return fs.writeFile(filepath, base64, `base64`, (err) => {
      if (err) return next(err);

      return fs.stat(filepath, (statErr, stat) => {
        if (statErr) {
          return next(statErr);
        }

        req.files.image = {
          name: filename,
          path: filepath,
          type: `image/png`,
          size: stat ? stat.size : base64.length / 1.37,
        };
        delete req.body.base64image;

        return next();
      });
    });
  }

  return next();
};
