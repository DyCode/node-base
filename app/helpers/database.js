'use strict';

const config = require(`../../config`);
const mongoose = require(`mongoose`);

mongoose.Promise = require(`bluebird`);

const options = {
  server: { socketOptions: { keepAlive: 1 } },
};

mongoose.connect(config.db, options);

mongoose.connection.on(`connected`, () => {
  console.log(`Mongoose default connection open to ${config.db}`);
});

mongoose.connection.on(`error`, (err) => {
  console.log(`Mongoose default connection error: ${err}`);
});

mongoose.connection.on(`disconnected`, () => {
  console.log(`Mongoose default connection disconnected`);
});

process.on(`SIGINT`, () => {
  mongoose.connection.close(() => {
    console.log(`Mongoose default connection disconnected through app termination`);
    process.exit(0);
  });
});

module.exports = mongoose;
