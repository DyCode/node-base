'use strict';

const _ = require(`lodash`);
const mongoose = require(`mongoose`);
const Promise = require(`bluebird`);

require(`../models/OAuthAccessToken`);
require(`../models/OAuthAuthorizationCode`);
require(`../models/OAuthClient`);
require(`../models/OAuthRefreshToken`);
require(`../models/OAuthScope`);
require(`../models/User`);

const OAuthAccessToken = mongoose.model(`OAuthAccessToken`);
const OAuthAuthorizationCode = mongoose.model(`OAuthAuthorizationCode`);
const OAuthClient = mongoose.model(`OAuthClient`);
const OAuthRefreshToken = mongoose.model(`OAuthRefreshToken`);
const User = mongoose.model(`User`);

async function getAccessToken(bearerToken) {
  const accessToken = await OAuthAccessToken.findOne({ access_token: bearerToken });
  if (!accessToken) {
    return false;
  }

  const oauthClient = await OAuthClient.findOne({ user_id: accessToken.user_id });

  const user = await User.findOne({ _id: accessToken.user_id });

  accessToken.oauthclient = oauthClient;
  accessToken.user = user;

  return accessToken;
}

async function getClient(clientId, clientSecret) {
  const query = { client_id: clientId, client_secret: clientSecret };
  const client = await OAuthClient.findOne(query);

  if (!client) {
    return null;
  }

  client.grants = [
    `authorization_code`,
    `password`,
    `refresh_token`,
    `client_credentials`,
  ];

  client.redirectUrls = [client.redirect_uri];

  return client;
}


async function getUser(id, password) {
  const user = await User.findOne({ _id: id });
  if (!user) {
    return false;
  }

  const isPasswordMatch = await user.matchingPassword(user.passwordHash, password);

  return isPasswordMatch ? user : false;
}

async function revokeAuthorizationCode(code) {
  const rCode = await OAuthAuthorizationCode.findOne({ auhtorization_code: code });
  if (!rCode) {
    return false;
  }

  rCode.expires_at = new Date();

  return rCode;
}

async function revokeToken(token) {
  const refreshToken = await OAuthRefreshToken.findOne({ refresh_token: token });

  if (!refreshToken) {
    return false;
  }

  const removed = await refreshToken.remove();

  return !!removed;
}


async function saveToken(token, client, user) {
  await Promise.all([
    OAuthAccessToken.create({
      access_token: token.accessToken,
      expires: token.accessTokenExpiresAt,
      oauth_client_id: client.id,
      user_id: user.id,
      scope: token.scope,
    }),

    token.refreshToken ? OAuthRefreshToken.create({
      refresh_token: token.refreshToken,
      expires: token.refreshTokenExpiresAt,
      oauth_client_id: client.id,
      user_id: user.id,
      scope: token.scope,
    }) : [],
  ]);

  const finalResult = {
    client,
    user,
    access_token: token.accessToken, // proxy
    refresh_token: token.refreshToken, // proxy
  };

  return _.assign(finalResult, token);
}

async function getAuthorizationCode(code) {
  const authCode = await OAuthAuthorizationCode.findOne({ authorization_code: code });
  if (!authCode) {
    return false;
  }

  const client = await OAuthClient.findOne({ _id: authCode.oauth_client_id });
  const user = await User.findOne({ _id: authCode.user_id });

  return {
    code,
    client,
    user,
    expires_at: authCode.expires,
    redirect_uri: client.redirect_uri,
    scope: authCode.scope,
  };
}

async function saveAuthorizationCode(code, client, user) {
  const data = {
    expires: code.expires_at,
    oauth_client_id: client.id,
    authorization_code: code.authorization_code,
    user: user.id,
    scope: code.scope,
  };

  await OAuthAuthorizationCode.create(data);

  code.code = code.authorization_code;

  return code;
}

async function getUserFromClient(client) {
  const query = _.pick(client, [`client_id`, `client_secret`]);
  const retrievedClient = await OAuthClient.findOne(query);

  if (!retrievedClient) {
    return false;
  }

  const user = await User.findOne({ _id: retrievedClient.user_id });
  if (!user) {
    return false;
  }

  return user;
}

async function getRefreshToken(refreshToken) {
  if (!refreshToken || refreshToken === `undefined`) {
    return false;
  }

  const token = await OAuthRefreshToken.findOne({ refresh_token: refreshToken });
  if (!token) {
    return false;
  }

  const user = await User.findOne({ _id: token.user_id });
  const client = await OAuthClient.findOne({ _id: token.oauth_client_id });

  return {
    user: user || {},
    client: client || {},
    refresh_token_expires_at: token ? new Date(token.expires) : null,
    refresh_token: refreshToken,
    scope: token.scope || ``,
  };
}

function validateScope(token, scope) {
  console.log(`validateScope`, token, scope);
  // return token.scope === scope;

  /* Bypassing scope */
  return true;
}

module.exports = {
  // generateOAuthAccessToken, optional - used for jwt
  // generateAuthorizationCode, optional
  // generateOAuthRefreshToken, - optional
  getAccessToken,
  getAuthorizationCode, // getOAuthAuthorizationCode renamed to,
  getClient,
  getRefreshToken,
  getUser,
  getUserFromClient,
  // grantTypeAllowed, Removed in oauth2-server 3.0
  revokeAuthorizationCode,
  revokeToken,
  saveToken, // saveOAuthAccessToken, renamed to
  saveAuthorizationCode, // renamed saveOAuthAuthorizationCode,
  validateScope,
};
