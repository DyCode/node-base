'use strict';

const passport = require(`passport`);

require(`../models/user`);

passport.serializeUser((user, done) => done(null, user));

passport.deserializeUser((user, done) => done(null, user));
