'use strict';

const config = require(`../../config`);

const mailConfig = config.sendgrid;
const Sendgrid = require(`sendgrid`)(mailConfig.apiKey);

const mailController = {
  send(opt, callback) {
    const mail = opt.info || {};
    const params = mail.customparams || {};

    const mailTo = [];
    const mailFrom = { email: mail.from };
    const subject = mail.subject;
    const substitutions = {
      '<%code%>': params.forgotPasswordCode || ``,
      '<%app_name%>': `Loyalty Club`,
    };

    mailTo.push({ email: mail.to });

    const personalization = {
      subject,
      substitutions,
      to: mailTo,
    };

    const request = Sendgrid.emptyRequest({
      method: `POST`,
      path: `/v3/mail/send`,
      body: {
        personalizations: [personalization],
        from: mailFrom,
        template_id: `cebe1767-b9d8-4174-9bbe-c9d084132534`,
      },
    });

    Sendgrid.API(request, callback);
  },
};


module.exports = mailController;
