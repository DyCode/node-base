'use strict';

const OAuthServer = require(`oauth2-server`);
const BasicAuth = require(`basic-auth`);

const Request = OAuthServer.Request;
const Response = OAuthServer.Response;

const model = require(`./oauth-model`);

const oauth = new OAuthServer({
  model,
  grants: [`password`, `client_credentials`, `refresh_token`],
  debug: true,
  accessTokenLifeTime: null,
  refreshTokenLifeTime: null,
});

function authenticate(options) {
  options = options || {};

  return function authenticateMiddleware(req, res, next) {
    const request = new Request({
      headers: { authorization: req.headers.authorization },
      method: req.method,
      query: req.query,
      body: req.body,
    });

    const response = new Response(res);

    oauth.authenticate(request, response, options)
      .then((authToken) => {
        req.token = authToken;
        req.user = authToken.user;
        next();
      })
      .catch((err) => {
        res.status(err.code || 500).json(err);
      });
  };
}

function token(options) {
  options = options || {};

  return function tokenMiddleware(req, res, next) {
    const request = new Request({
      headers: req.headers,
      method: req.method,
      query: req.query,
      body: req.body,
    });

    const response = new Response(res);

    oauth.token(request, response, options)
      .then((authToken) => {
        // Request is authorized.
        req.token = authToken;
        req.user = authToken.user;
        next();
      })
      .catch((err) => {
        res.status(err.code || 500).json(err);
      });
  };
}

function basic() {
  return function basicMiddleware(req, res, next) {
    const request = new Request({
      headers: req.headers,
      method: req.method,
      query: req.query,
      body: req.body,
    });

    const credentials = BasicAuth(request);
    const client = model.getClient(credentials.name, credentials.pass);

    client.then((clientData) => {
      if (!clientData.client_id) {
        return res.status(400).send({
          code: 400,
          name: `invalid_client`,
          message: `Invalid client: client credentials are invalid`,
        });
      }

      return next();
    }).catch(ex => next(ex));
  };
}

module.exports = {
  authenticate,
  token,
  basic,
};
