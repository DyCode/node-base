'use strict';

const mongoose = require(`mongoose`);

const fields = {
  authorization_code: String,
  expires: Date,
  redirect_uri: String,
  scope: String,
  user_id: mongoose.SchemaTypes.ObjectId,
  oauth_client_id: mongoose.SchemaTypes.ObjectId,
};

const OAuthAuthorizationCodeSchema = new mongoose.Schema(fields);

module.exports = mongoose.model(`OAuthAuthorizationCode`, OAuthAuthorizationCodeSchema);

