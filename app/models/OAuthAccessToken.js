'use strict';

const mongoose = require(`mongoose`);

const fields = {
  access_token: String,
  expires: Date,
  scope: String,
  user_id: mongoose.SchemaTypes.ObjectId,
  oauth_client_id: mongoose.SchemaTypes.ObjectId,
};

const OAuthAccessTokenSchema = new mongoose.Schema(fields);

module.exports = mongoose.model(`OAuthAccessToken`, OAuthAccessTokenSchema);
