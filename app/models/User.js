'use strict';

const mongoose = require(`mongoose`);
const Promise = require(`bluebird`);

const bcrypt = Promise.promisifyAll(require(`bcryptjs`));

const fields = {
  email: String,
  fullname: String,
  password_hash: String,
  forgot_password_code: String,
  provider: mongoose.SchemaTypes.Mixed,
};

const UserSchema = new mongoose.Schema(fields);

UserSchema.methods.toJSON = function toJSON(obj) {
  const object = obj;
  delete object.password_hash;

  return object;
};

UserSchema.methods.hashingPassword = function hashingPassword(password) {
  return bcrypt.hashSync(password, 10);
};

UserSchema.methods.matchingPassword = function matchingPassword(storedPassword, password) {
  return bcrypt.compareAsync(password, storedPassword);
};

module.exports = mongoose.model(`User`, UserSchema);
