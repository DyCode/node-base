'use strict';

const mongoose = require(`mongoose`);

const fields = {
  refresh_token: String,
  expires: Date,
  scope: String,
  user_id: mongoose.SchemaTypes.ObjectId,
  oauth_client_id: mongoose.SchemaTypes.ObjectId,
};

const OAuthRefreshTokenSchema = new mongoose.Schema(fields);

module.exports = mongoose.model(`OAuthRefreshToken`, OAuthRefreshTokenSchema);
