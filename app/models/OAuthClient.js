'use strict';

const mongoose = require(`mongoose`);

const fields = {
  name: String,
  client_id: String,
  client_secret: String,
  redirect_uri: String,
  grant_types: String,
  scope: String,
  user_id: mongoose.SchemaTypes.ObjectId,
};

const OAuthClientSchema = new mongoose.Schema(fields);

module.exports = mongoose.model(`OAuthClient`, OAuthClientSchema);
