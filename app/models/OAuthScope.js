'use strict';

const mongoose = require(`mongoose`);

const fields = {
  scope: String,
  is_default: Boolean,
};

const OAuthScopeSchema = new mongoose.Schema(fields);

module.exports = mongoose.model(`OAuthScope`, OAuthScopeSchema);
