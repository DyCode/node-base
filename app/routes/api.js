'use strict';

const express = require(`express`);
const OAuth = require(`../helpers/oauth`);
const AuthController = require(`../controllers/Authentication`);

const router = express.Router();

router.get(`/`, (req, res) => {
  res.ok(`Node API. Maybe`);
});

router.post(`/register`, OAuth.basic(), AuthController.register);
router.post(`/signinwith`, OAuth.basic(), AuthController.signInWith);
router.post(`/signin`, OAuth.token(), AuthController.signin);

router.get(`/forgot`, OAuth.basic(), AuthController.requestForgotPassword);
router.post(`/forgot`, OAuth.basic(), AuthController.validateForgotPassword);

router.post(`/password`, OAuth.basic(), AuthController.changePassword);

router.get(`/auth-test`, OAuth.authenticate(), (req, res) => {
  res.ok(`Auth Test. Completed`);
});

module.exports = router;
