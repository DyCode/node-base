'use strict';

exports.route = function asyncRoute(middleware) {
  return async function asyncMiddleWare(req, res, next) {
    try {
      await middleware(req, res, next);
    } catch (err) {
      next(err);
    }
  };
};
