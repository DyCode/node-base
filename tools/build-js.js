'use strict';

const config = require(`../config`);
const fs = require(`fs`);
const uglify = require(`uglify-js`);
const flow = require(`lodash/fp/flow`);

const js = [
  `lodash`,
  `jquery`,
  `datatables`,
  `jquery-form`,
  `jquery-validation`,
  `sweetalert`,
];

const minify = flow(
  require.resolve,
  uglify.minify,
  result => result.code
);

/* eslint lodash-fp/no-for-each:0 */
js.forEach((name) => {
  fs.writeFileSync(`${config.appDir}/static/${name}.min.js`, minify(name));
});
