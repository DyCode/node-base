'use strict';

const path = require(`path`);

const root = `${path.join(__dirname, `..`)}/`;
const arg = require(`minimist`)(process.argv);

const formatter = require(path.join(
  require.resolve(`eslint`),
  `../formatters`,
  Array.isArray(arg.f) ? arg.f[0] : `stylish`
));

module.exports = results => formatter(results.map((result) => {
  result.filePath = result.filePath.replace(root, ``);

  return result;
}));
