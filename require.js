'use strict';

const { name } = require(`./package.json`);

process.env.DEBUG = process.env.DEBUG || name;
const debug = require(`debug`);
const chalk = require(`chalk`);

if (!process.execArgv.find(arg => arg === `--inspect`)) {
  const log = debug(name);
  log.log = console.log.bind(console);

  console.log = (...args) => {
    const origin = (new Error()).stack.split(`\n`)[2];
    const location = origin
      .slice(origin.indexOf(__dirname) + __dirname.length + 1)
      .replace(`)`, ``)
      .trim();

    log(...args, chalk.gray(location));
  };

  console.error = (...args) => {
    const origin = (new Error()).stack.split(`\n`)[2];
    const location = origin
      .slice(origin.indexOf(__dirname) + __dirname.length + 1)
      .replace(`)`, ``)
      .trim();

    log(...args, chalk.red(location));
  };
}
