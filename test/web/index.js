'use strict';

const assert = require(`assert`);
const test = require(`selenium-webdriver/testing`);
const webdriver = require(`selenium-webdriver`);

test.describe(`Nodejs app website`, function testApp() {
  this.timeout(20000);
  let driver;

  test.before(() => {
    driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
  });

  test.it(`Shoule have text: Nodejs App`, () => {
    driver.get(`http://localhost:8000`);
    const helloText = driver.findElement(webdriver.By.id(`hello`));

    helloText.getText().then((value) => {
      assert.equal(value, `Nodejs App`);
    });
  });

  test.after(() => {
    driver.quit();
  });
});
