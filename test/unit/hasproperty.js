'use strict';

const assert = require(`assert`);
const Utils = require(`../../app/services/Utils`);

describe(`hasProperty`, () => {
  it(`return true if has all properties`, () => {
    assert(Utils.hasProperty({ one: `one`, two: `two` }, [`one`]), `Object has "{'one':'one', 'two':'two'}" has property with name one`);
  });

  it(`return false if has missing property`, () => {
    assert.equal(Utils.hasProperty({ one: `one`, two: `two` }, [`one`, `three`]), false, `Object has "{'one':'one', 'two':'two'}" don't have property with name three`);
  });
});
