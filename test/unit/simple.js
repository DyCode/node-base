'use strict';

const assert = require(`assert`);

describe(`Simple test`, () => {
  it(`bar equal bar`, () => {
    assert.equal(`bar`, `bar`, `\`bar\` equal \`bar\``);
  });

  it(`foo equal foo`, () => {
    assert.equal(`foo`, `foo`, `\`foo\` equal \`foo\``);
  });

  it(`foo not equal bar`, () => {
    assert.notEqual(`foo`, `bar`, `\`foo\` not equal \`bar\``);
  });
});
