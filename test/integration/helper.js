'use strict';

const debug = require(`debug`)(`test`);
const Mongoose = require(`mongoose`);
/* eslint no-restricted-syntax: 0 */
const config = require(`../../config/${process.env.NODE_ENV || ``}`);

before((done) => {
  function clearDB() {
    for (const i in Mongoose.connection.collections) {
      if (Mongoose.connection.collections[i]) {
        Mongoose.connection.collections[i].remove();
      }
    }

    return done();
  }

  const options = {
    server: { socketOptions: { keepAlive: 1 } },
  };

  Mongoose.connect(config.db, options);

  Mongoose.connection.on(`connected`, () => {
    debug(`Reset mongo database`);
    clearDB();
  });
});
