'use strict';

const assert = require(`assert`);
const test = require(`selenium-webdriver/testing`);
const webdriver = require(`selenium-webdriver`);

test.describe(`Google Search`, function testGoogle() {
  this.timeout(20000);

  test.it(`should work chrome`, () => {
    const driver = new webdriver.Builder()
      .withCapabilities(webdriver.Capabilities.phantomjs())
      .build();

    driver.get(`http://www.google.com`);
    const searchBox = driver.findElement(webdriver.By.name(`q`));

    searchBox.sendKeys(`dycode`);
    searchBox.getAttribute(`value`).then((value) => {
      assert.equal(value, `dycode`);
    });

    driver.quit();
  });

  test.it(`should work firefox`, () => {
    const driver = new webdriver.Builder()
      .withCapabilities(webdriver.Capabilities.firefox())
      .build();

    driver.get(`http://www.google.com`);
    const searchBox = driver.findElement(webdriver.By.name(`q`));

    searchBox.sendKeys(`dycode`);
    searchBox.getAttribute(`value`).then((value) => {
      assert.equal(value, `dycode`);
    });

    driver.quit();
  });

  test.it(`should work phantomjs`, () => {
    const driver = new webdriver.Builder()
      .withCapabilities(webdriver.Capabilities.phantomjs())
      .build();

    driver.get(`http://www.google.com`);
    const searchBox = driver.findElement(webdriver.By.name(`q`));

    searchBox.sendKeys(`dycode`);
    searchBox.getAttribute(`value`).then((value) => {
      assert.equal(value, `dycode`);
    });

    driver.quit();
  });
});
