'use strict';

const config = require(`./${process.env.NODE_ENV || ``}`);
const path = require(`path`);
const merge = require(`lodash/fp/merge`);

module.exports = merge({
  port: process.env.PORT || 8000,

  root: path.join(__dirname, `..`),

  dir: {
    upload: `/assets/upload`,
  },

  db: `mongodb://localhost`,

  sendgrid: {
    apiKey: ``,
  },

  i18n: {
    defaultLocale: `en_US`,
  },

  view: {
    cache: false,
  },

  cookie: {
    secret: `exposed`,
  },
}, config);
