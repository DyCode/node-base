'use strict';

const config = require(`./config`);
const { name } = require(`./package.json`);

const express = require(`express`);

const app = express();
const helmet = require(`helmet`);
const swig = require(`swig`);
const cookieParser = require(`cookie-parser`);
const bodyParser = require(`body-parser`);
const session = require(`express-session`);
const passport = require(`passport`);
const flash = require(`connect-flash`);
const i18n = require(`i18n`);
const MongoStore = require(`connect-mongo`)(session);
const Mongoose = require(`./app/helpers/database`);
const routes = require(`./app/routes`);

i18n.configure({
  locales: [`en_US`, `id`],
  directory: `${__dirname}/app/locales`,
  updateFiles: false,
});

// view engine setup
swig.setDefaults(config.view);
app.engine(`swig`, swig.renderFile);
app.set(`views`, `${__dirname}/app/views`);
app.set(`view engine`, `swig`);
app.disable(`x-powered-by`);

app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// initialize
app.use(session({
  name,
  resave: false,
  saveUninitialized: false,
  secret: config.cookie.secret,
  autoReconnect: true,
  maxAge: new Date(Date.now() + 3600000),
  store: new MongoStore({
    mongooseConnection: Mongoose.connection,
    collection: `${name}_sessions`,
  }),
}));

app.use(express.static(`${__dirname}/assets`));
app.use(express.static(`${__dirname}/bower_components`));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(i18n.init);
app.use(routes);

module.exports = app;
